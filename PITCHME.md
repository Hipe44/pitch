## Cloud services
---
 Lista serviços pesquisados
<table>
  <tr>
    <th style="font-size:0.6em;">Fonte</th>
    <th style="font-size:0.6em;">Nome</th>
  </tr>
  <tr class="fragment">
    <td style="font-size:0.6em;">Google</td>
    <td style="font-size:0.6em;">Compute engine</td>
  </tr>
  <tr class="fragment">
    <td style="font-size:0.6em;">Google</td>
    <td style="font-size:0.6em;">GKE Cluster</td>
  </tr>
  <tr class="fragment">
    <td style="font-size:0.6em;">Google</td>
    <td style="font-size:0.6em;">Cloud Storage</td>
  </tr>
  <tr class="fragment">
    <td style="font-size:0.6em;">Google</td>
    <td style="font-size:0.6em;">Cloud SQL Second Generation</td>
  </tr>
  <tr class="fragment">
    <td style="font-size:0.6em;">Google</td>
    <td style="font-size:0.6em;">Cloud Endpoints</td>
  </tr>
</table>

---
<table>
  <tr>
    <th style="font-size:0.6em;">Fonte</th>
    <th style="font-size:0.6em;">Nome</th>
  </tr>
  <tr class="fragment">
    <td style="font-size:0.6em;">Amazon</td>
    <td style="font-size:0.6em;">Amazon EC2</td>
  </tr>
  <tr class="fragment">
    <td style="font-size:0.6em;">Amazon</td>
    <td style="font-size:0.6em;">Amazon Glacier</td>
  </tr>
  <tr class="fragment">
    <td style="font-size:0.6em;">Amazon</td>
    <td style="font-size:0.6em;">Amazon Relational Database Service (RDS)</td>
  </tr>
</table>
---
<table>
  <tr>
    <th style="font-size:0.6em;">Fonte</th>
    <th style="font-size:0.6em;">Nome</th>
  </tr>
  <tr class="fragment">
    <td style="font-size:0.6em;">Microsoft</td>
    <td style="font-size:0.6em;">Virtual machine (Linux)</td>
  </tr>
  <tr class="fragment">
    <td style="font-size:0.6em;">Microsoft</td>
    <td style="font-size:0.6em;">Azure Database for PostgreSQL</td>
  </tr>
  <tr class="fragment">
    <td style="font-size:0.6em;">Microsoft</td>
    <td style="font-size:0.6em;">Traffic Manager</td>
  </tr>
  <tr class="fragment">
    <td style="font-size:0.6em;">Microsoft</td>
    <td style="font-size:0.6em;">Storage</td>
  </tr>
</table>
---
<span class="fragment" style="font-size:0.6em;">Obviamente que existem alguns que são "idênticos" entre plataformas.</span>
<br>
<span class="fragment" style="font-size:0.6em;">Procurei ver o que será construido como núcleo.</span>
<br>
<span class="fragment">Serviços / api's como firebase, Maps Api ,Stripes , entre outros são adicionados com a necessidade durante o processo de desenvolvimento.</span>


---
<span class="fragment" style="font-size:0.6em;">O funcionamento aparenta ser parecido nas três fontes de serviços.</span><br>

<span class="fragment" style="font-size:0.6em;">Aqui todos sabemos qual é o favorito á partida.</span>

---

<span class="fragment" style="font-size:0.6em;">O funcionamento aparenta ser bastante parecido pelas três.</span><br>
<span class="fragment" style="font-size:0.6em;">Pelos exemplos a dificuldade de utilização das coisas mais "basicas" nao mostra ser complicado em nenhuma</span><br>

---

<span class="fragment" style="font-size:0.6em;">Claro que a dificuldade não é tudo que importa. </span><br>
<span class="fragment" style="font-size:0.6em;">Felizmente todos tinham uma calculadora para estimar os custos mensais.</span><br>

---
<span class="fragment" style="font-size:0.6em;">Para os seguintes testes usei especificações parecidas, para VM's 2cores 4/3.5 Ram, storage 350 GB e base de dados 10GB  </span><br>
<span class="fragment" style="font-size:0.4em;">(Não é um teste 100% real)</span><br>

---

![Image-Absolute](https://i.gyazo.com/757d620582ab3809faa639b817894c2d.png)

---

![Image-Absolute](https://i.gyazo.com/d8b4e313fa3775526f5def221835b155.png)
---

![Image-Absolute](https://i.gyazo.com/d1812a2bd71b55e554035c40d4ac25e4.png)
---

![Image-Absolute](https://i.gyazo.com/ebf6b7266ec74946b26fac98042fdd9b.png)
